// ==UserScript==
// @name         打印页面指定元素
// @namespace    http://chrome.heretreeli.com/
// @version      0.1.3.2
// @description  打印页面指定元素，print html special element
// @author       You
// @match        *://*/*
// @grant        none
// ==/UserScript==

var global = this;
(function () {
    if (!global.jQuery || !global.$) {
        var script1 = document.createElement('script');
        script1.onload = function () {
            //do stuff with the script
        };
        script1.src = 'http://code.jquery.com/jquery-latest.js';

        document.body.appendChild(script1);
    }

    var script2 = document.createElement('script');
    script2.onload = function () {
        //do stuff with the script
    };
    //script2.src = 'https://gitee.com/itech/printThis.js/raw/master/printThis.js';
    script2.src = 'https://gitee.com/itech/printThis.js/raw/v1.14.0/printThis.js';

    document.body.appendChild(script2);
})();